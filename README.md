# Repositorio plantilla: "Triángulo"

Para entregar este ejercicio, crea una bifurcación (fork) de este repositorio, y sube a él tu solución. Puedes consultar el [enunciado](https://gitlab.etsit.urjc.es/cursoprogram/materiales/-/blob/main/practicas/ejercicios/README.md#ordenpalabras), que incluye la fecha de entrega.

Entrega:
[sortwords.py](https://gitlab.etsit.urjc.es/m.jimenezh.2022/ordenpalabras/-/blob/main/sortwords.py)
